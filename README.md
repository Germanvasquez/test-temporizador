
**instalacion**
**El proyecto esta creado con  el framework laravel **

*German Vasquez, vasquez93g3r@gmail.com*
**clonar repo**


```
$ git clone url_miproyect && cd url_labbe
```
**configurar proyecto**
```
$ composer install
```

```
$ cp .env-example .env
```

```
$ php artisan key:generate
```
```
$ npm install
```
**Levantar el servidor local**

```
$ php artisan serve
```
**Nos dara una ruta como esta: http://127.0.0.1:8000**
