<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('test', 'test') }} | @yield('title')</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">


</head>
<body>
<div id="app">
    <section class="hero is-primary is-bold is-fullheight">
        <!-- Hero header: will stick at the top -->
        <div class="hero-head">
            <header class="nav">

            </header>
        </div>




        <!-- Hero content: will be in the middle -->
        <div class="hero-body">
            <div class="container">
                @yield('content')
            </div>
        </div>

    </section>




</div>
<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>

</body>
</html>



<style>

    .hero-body {
        padding-top: 20px;

    }

</style>
